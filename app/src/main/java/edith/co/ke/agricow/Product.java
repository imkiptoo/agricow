package edith.co.ke.agricow;

public class Product {
    private String image, name, color, stock;

    public Product() {
    }

    public Product(String image, String name, String color, String stock) {
        this.image = image;
        this.name = name;
        this.color = color;
        this.stock = stock;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }

    public String getStock() {
        return stock;
    }
    public void setStock(String stock) {
        this.stock = stock;
    }
}

