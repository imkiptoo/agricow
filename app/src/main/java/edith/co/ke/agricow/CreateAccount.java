package edith.co.ke.agricow;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CreateAccount extends AppCompatActivity {
    private GoogleApiClient mClient;
    private DatabaseReference mDatabase;
    private Spinner spCounty;
    private EditText txName, txPhoneNumber, txIDNumber;
    private String strIDNumber, strName, strPhoneNumber /*strCounty*/;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("AgriCow");

        Button btnCreateAccount =  (Button) findViewById(R.id.btnCreateAccount);
        txName = (EditText) findViewById(R.id.txName);
        txPhoneNumber = (EditText) findViewById(R.id.txPhoneNumber);
        txIDNumber = (EditText) findViewById(R.id.txIDNumber);
        /*spCounty = (Spinner) findViewById(R.id.spCounty);*/

        /*ArrayAdapter<CharSequence> countiesAdapter = ArrayAdapter.createFromResource(this, R.array.counties_array, android.R.layout.simple_spinner_dropdown_item);
        countiesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCounty.setAdapter(countiesAdapter);*/

        mClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        strIDNumber = txIDNumber.getText().toString();
        strName = txName.getText().toString();
        strPhoneNumber = txPhoneNumber.getText().toString();
        /*strCounty = spCounty.getSelectedItem().toString();*/

        mDatabase = FirebaseDatabase.getInstance().getReference();

        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertNewUser(strIDNumber, strName, strPhoneNumber/*strCounty*/);

                MaterialDialog dataInsertedSuccessfully = new MaterialDialog.Builder(CreateAccount.this)
                        .title("Success!")
                        .content("Account has been created successfully.")
                        .canceledOnTouchOutside(false)
                        .positiveText("Close")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                ListProducts.prefs.edit().putBoolean("accountcreated", false).apply();
                                startActivity(new Intent(CreateAccount.this, ListProducts.class));
                                finish();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_account, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            MaterialDialog callSeller = new MaterialDialog.Builder(CreateAccount.this)
                    .title("AgriCow Help")
                    .content("This mobile application helps you find cattle to buy and also to sell.")
                    .canceledOnTouchOutside(false)
                    .negativeText("Close")
                    .show();
        } else if (id == R.id.action_create_acc) {
            startActivity(new Intent(CreateAccount.this, CreateAccount.class));
        } else if (id == R.id.action_exit) {
            MaterialDialog dialog = new MaterialDialog.Builder(CreateAccount.this)
                    .title("Exit")
                    .content("Do you want to exit?")
                    .positiveText("Yes")
                    .negativeText("Cancel")
                    .canceledOnTouchOutside(false)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                            homeIntent.addCategory( Intent.CATEGORY_HOME );
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(homeIntent);
                            finish();
                        }
                    })
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void insertNewUser(String userId, String name, String phoneNo /*String county*/) {
        User user = new User(userId, name, phoneNo /*county*/);
        mDatabase.child("users").child("0").setValue(user);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mClient.connect();
    }
    @Override
    protected void onStop() {
        mClient.disconnect();
        super.onStop();
    }
}
