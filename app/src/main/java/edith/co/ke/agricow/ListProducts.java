package edith.co.ke.agricow;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

public class ListProducts extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    int PLACE_PICKER_REQUEST = 1;
    private GoogleApiClient mClient;
    TextView textView;
    private DatabaseReference mDatabase;
    public static SharedPreferences prefs = null;

    private List<Product> productList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ProductsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (prefs.getBoolean("accountcreated", true)) {
                    MaterialDialog createAccountFirstDialog = new MaterialDialog.Builder(ListProducts.this)
                            .title("Create account first")
                            .content("You need to create an account first before you can add any products.")
                            .canceledOnTouchOutside(false)
                            .positiveText("Create account")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivity(new Intent(ListProducts.this, CreateAccount.class));
                                }
                            })
                            .show();
                } else {
                    startActivity(new Intent(ListProducts.this, AddProduct.class));
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new ProductsAdapter(productList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Product product = productList.get(position);
                Intent i = new Intent(ListProducts.this, ViewProduct.class);
                i.putExtra("breed", product.getName());
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareProductData();
    }


    private void prepareProductData() {
        //String image, String name, String color, String stock
        Product product = new Product("holstein", "Holstein", "black_and_white", "11");
        productList.add(product);
        product = new Product("charolais", "Charolias", "brown_and_white", "17");
        productList.add(product);
        product = new Product("belted_galloway", "Belted Galloway", "black_and_white", "17");
        productList.add(product);
        product = new Product("angus", "Angus", "black", "34");
        productList.add(product);
        product = new Product("brahman", "Brahman", "black_and_white", "8");
        productList.add(product);
        product = new Product("dexter", "Dexter", "black", "6");
        productList.add(product);
        product = new Product("gelbvieh", "Gelbvieh", "brown", "14");
        productList.add(product);
        product = new Product("hereford", "Hereford", "brown_and_white", "21");
        productList.add(product);
        product = new Product("limousin", "Limousin", "brown_and_white", "51");
        productList.add(product);
        product = new Product("piedmontese", "Piedmontese", "brown", "4");
        productList.add(product);
        product = new Product("red_angus", "Red Angus", "brown", "14");
        productList.add(product);
        product = new Product("scottish_highland", "Scottish Highland", "brown_and_white", "9");
        productList.add(product);
        product = new Product("shorthorn", "Shorthorn", "brown", "47");
        productList.add(product);
        product = new Product("simmental", "Simmental", "brown_and_white", "23");
        productList.add(product);
        product = new Product("texas_longhorn", "Texas Longhorn", "black_and_white", "6");
        productList.add(product);
        product = new Product("watusi", "Watusi", "brown", "8");
        productList.add(product);

        mAdapter.notifyDataSetChanged();
    }


    private void writeNewUser(String userId, String name, String email) {
        //User user = new User(name, email);
        //mDatabase.child("users").child(userId).setValue(user);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            MaterialDialog dialog = new MaterialDialog.Builder(ListProducts.this)
                    .title("Exit")
                    .content("Do you want to exit?")
                    .positiveText("Yes")
                    .negativeText("Cancel")
                    .canceledOnTouchOutside(false)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                            homeIntent.addCategory( Intent.CATEGORY_HOME );
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(homeIntent);
                            finish();
                        }
                    })
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            MaterialDialog callSeller = new MaterialDialog.Builder(ListProducts.this)
                    .title("AgriCow Help")
                    .content("This mobile application helps you find cattle to buy and also to sell.")
                    .canceledOnTouchOutside(false)
                    .negativeText("Close")
                    .show();
        } else if (id == R.id.action_create_acc) {
            startActivity(new Intent(ListProducts.this, CreateAccount.class));
        } else if (id == R.id.action_exit) {
            MaterialDialog dialog = new MaterialDialog.Builder(ListProducts.this)
                    .title("Exit")
                    .content("Do you want to exit?")
                    .positiveText("Yes")
                    .negativeText("Cancel")
                    .canceledOnTouchOutside(false)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                            homeIntent.addCategory( Intent.CATEGORY_HOME );
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(homeIntent);
                            finish();
                        }
                    })
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
        } else if (id == R.id.nav_add_product) {
            startActivity(new Intent(ListProducts.this, AddProduct.class));
        } else if (id == R.id.nav_view_products) {
            //startActivity(new Intent(ListProducts.this, ListProducts.class));
        } else if (id == R.id.nav_create_account) {
            startActivity(new Intent(ListProducts.this, CreateAccount.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mClient.connect();
    }
    @Override
    protected void onStop() {
        mClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                String latitude = String.valueOf(place.getLatLng().latitude);
                String longitude = String.valueOf(place.getLatLng().longitude);
                String address = String.format("%s", place.getAddress());
                stBuilder.append("Name: ");
                stBuilder.append(placename);
                stBuilder.append("\n");
                stBuilder.append("Latitude: ");
                stBuilder.append(latitude);
                stBuilder.append("\n");
                stBuilder.append("Logitude: ");
                stBuilder.append(longitude);
                stBuilder.append("\n");
                stBuilder.append("Address: ");
                stBuilder.append(address);
                textView.setText(stBuilder.toString());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        prefs = getSharedPreferences("edith.co.ke.agricow", MODE_PRIVATE);
    }
}
