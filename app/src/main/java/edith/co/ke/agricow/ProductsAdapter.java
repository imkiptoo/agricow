package edith.co.ke.agricow;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {
    private List<Product> productsList;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, stock;
        ImageView image, color;

        MyViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            name = (TextView) itemLayoutView.findViewById(R.id.name);
            stock = (TextView) itemLayoutView.findViewById(R.id.stock);
            image = (ImageView) itemLayoutView.findViewById(R.id.image);
            color = (ImageView) itemLayoutView.findViewById(R.id.color);
        }
    }

    ProductsAdapter(List<Product> productsList) {
        this.productsList = productsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Product product = productsList.get(position);
        holder.name.setText(product.getName());
        holder.stock.setText(product.getStock() +" in stock");
        switch(product.getImage()){
            case "angus":
                holder.image.setImageResource(R.drawable.angus);
                break;
            case "belted_galloway":
                holder.image.setImageResource(R.drawable.belted_galloway);
                break;
            case "brahman":
                holder.image.setImageResource(R.drawable.brahman);
                break;
            case "charolais":
                holder.image.setImageResource(R.drawable.charolais);
                break;
            case "dexter":
                holder.image.setImageResource(R.drawable.dexter);
                break;
            case "gelbvieh":
                holder.image.setImageResource(R.drawable.gelbvieh);
                break;
            case "hereford":
                holder.image.setImageResource(R.drawable.hereford);
                break;
            case "holstein":
                holder.image.setImageResource(R.drawable.holstein);
                break;
            case "limousin":
                holder.image.setImageResource(R.drawable.limousin);
                break;
            case "piedmontese":
                holder.image.setImageResource(R.drawable.piedmontese);
                break;
            case "red_angus":
                holder.image.setImageResource(R.drawable.red_angus);
                break;
            case "scottish_highland":
                holder.image.setImageResource(R.drawable.scottish_highland);
                break;
            case "shorthorn":
                holder.image.setImageResource(R.drawable.shorthorn);
                break;
            case "simmental":
                holder.image.setImageResource(R.drawable.simmental);
                break;
            case "texas_longhorn":
                holder.image.setImageResource(R.drawable.texas_longhorn);
                break;
            case "watusi":
                holder.image.setImageResource(R.drawable.watusi);
                break;
        }
        if(product.getColor().equals("black")){
            holder.color.setImageResource(R.drawable.black);
        } else if(product.getColor().equals("black_and_white")){
            holder.color.setImageResource(R.drawable.black_and_white);
        } else if(product.getColor().equals("brown")){
            holder.color.setImageResource(R.drawable.brown);
        } else {
            holder.color.setImageResource(R.drawable.brown_and_white);
        }
    }

    @Override
    public int getItemCount() {
        return productsList.size();
    }
}
