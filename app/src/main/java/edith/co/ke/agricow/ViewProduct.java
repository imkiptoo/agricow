package edith.co.ke.agricow;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class ViewProduct extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, NavigationView.OnNavigationItemSelectedListener {
    private GoogleMap mMap;
    private List<Seller> sellerList = new ArrayList<>();
    Seller seller;
    LatLng location;
    String breedHeader;
    int iterator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_product);

        final Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            breedHeader = bundle.getString("breed");
        } else {
            Toast.makeText(this,"Bundle null",Toast.LENGTH_LONG).show();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(breedHeader);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        seller = new Seller(1, "Clarabelle", "Holstein-Friesian", "Clarabelle is a holstein fresian.. description blah blah blah", "imagebase64", "7", "KES 45,000", "0714856987", -1.425187, 36.693651);
        sellerList.add(seller);
        seller = new Seller(2, "Henrietta", "Gurnsey", "Henrietta is a gurnsey cow.. description blah blah blah", "imagebase64", "3", "KES 45,000", "0714856987", -1.106931, 36.643127);
        sellerList.add(seller);
        seller = new Seller(3, "Astrabelle", "Gurnsey", "Henrietta is a gurnsey cow.. description blah blah blah", "imagebase64", "3", "KES 45,000", "0714856987", -1.258430, 36.767150);
        sellerList.add(seller);
    }

    /** Called when the map is ready. */
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        int height = 100;
        int width = 100;
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.ic_cow_location);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(iterator = 0; iterator < sellerList.size(); iterator++){
            location = new LatLng(sellerList.get(iterator).latitude, sellerList.get(iterator).longitude);
            builder.include(location);
            map.addMarker(new MarkerOptions()
                    .position(location)
                    .title(sellerList.get(iterator).name)
                    .snippet(seller.getPrice())
                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
            );
        }
        LatLngBounds bounds = builder.build();

        int mapWidth = getResources().getDisplayMetrics().widthPixels;
        int mapHeight = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (mapWidth * 0.30); // offset from edges of the map 30% of screen
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, mapWidth, mapHeight, padding);
        mMap.animateCamera(cu);

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent i = new Intent(ViewProduct.this, ItemBeingSoldDetails.class);

                System.out.println("ID is "+marker.getTitle());
                i.putExtra("id", marker.getTitle());
                startActivity(i);
            }
        });

        // Set a listener for marker click.
        mMap.setOnMarkerClickListener(this);
    }

    /**Called when the user clicks a marker. */
    @Override
    public boolean onMarkerClick(final Marker marker) {
        // Retrieve the data from the marker.
        Integer clickCount = (Integer) marker.getTag();
        // Check if a click count was set, then display the click count.
        if (clickCount != null) {
            clickCount = clickCount + 1;
            marker.setTag(clickCount);
            Toast.makeText(this,
                    marker.getTitle() +
                            " has been clicked " + clickCount + " times.",
                    Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            MaterialDialog callSeller = new MaterialDialog.Builder(ViewProduct.this)
                    .title("AgriCow Help")
                    .content("This mobile application helps you find cattle to buy and also to sell.")
                    .canceledOnTouchOutside(false)
                    .negativeText("Close")
                    .show();
        } else if (id == R.id.action_create_acc) {
            startActivity(new Intent(ViewProduct.this, CreateAccount.class));
        } else if (id == R.id.action_exit) {
            MaterialDialog dialog = new MaterialDialog.Builder(ViewProduct.this)
                    .title("Exit")
                    .content("Do you want to exit?")
                    .positiveText("Yes")
                    .negativeText("Cancel")
                    .canceledOnTouchOutside(false)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                            homeIntent.addCategory( Intent.CATEGORY_HOME );
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(homeIntent);
                            finish();
                        }
                    })
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(ViewProduct.this, ListProducts.class));
            finish();
        } else if (id == R.id.nav_add_product) {
            startActivity(new Intent(ViewProduct.this, AddProduct.class));
        } else if (id == R.id.nav_view_products) {
            startActivity(new Intent(ViewProduct.this, ListProducts.class));
        } else if (id == R.id.nav_create_account) {
            startActivity(new Intent(ViewProduct.this, CreateAccount.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

