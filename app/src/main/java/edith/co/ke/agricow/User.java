package edith.co.ke.agricow;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {

    public String name;
    public String phoneNo;
    /*public String county;*/
    public String userId;

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    /*public User() {
    }*/

    public User(String userId, String name, String phoneNo/*String county*/) {
        this.name = name;
        this.phoneNo = phoneNo;
        /*this.county = county;*/
        this.userId = userId;
    }
}