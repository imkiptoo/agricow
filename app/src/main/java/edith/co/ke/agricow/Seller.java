package edith.co.ke.agricow;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Seller {
    public int id;
    public String name;
    public String breed;
    public String description;
    public String image;
    public String age;
    public String price;
    public String contact;
    public double latitude;
    public double longitude;

    public Seller() {
    }

    public Seller(int id, String name, String breed, String description, String image, String age, String price, String contact, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.description = description;
        this.image = image;
        this.age = age;
        this.price = price;
        this.contact = contact;
        this.latitude = latitude; 
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }
    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getAge() {
        return age;
    }
    public void setAge(String age) {
        this.age = age;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getContact() {
        return contact;
    }
    public void setContact(String contact) {
        this.contact = contact;
    }

    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}