package edith.co.ke.agricow;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

public class ItemBeingSoldDetails extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    TextView tvCowsName, tvBreed, tvDescription, tvAge, tvPrice, tvSeller, tvContact, tvLocation;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_being_sold_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tvCowsName = (TextView) findViewById(R.id.tvCowsName);
        tvBreed = (TextView) findViewById(R.id.tvBreed);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvAge = (TextView) findViewById(R.id.tvAge);
        tvPrice = (TextView) findViewById(R.id.tvPrice);
        tvSeller = (TextView) findViewById(R.id.tvSeller);
        tvContact = (TextView) findViewById(R.id.tvContact);
        tvLocation = (TextView) findViewById(R.id.tvLocation);

        tvCowsName.setText("Clarabelle");
        tvBreed.setText("Holstein");
        tvDescription.setText("Clarabelle cow description blah blah blah n stuff");
        tvAge.setText("7");
        tvPrice.setText("69, 000");
        tvSeller.setText("Karura Farm Limited");
        tvContact.setText("0706405898");
        tvLocation.setText("12 Karura Road, Karura - Nairobi");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabCallSeller);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaterialDialog callSeller = new MaterialDialog.Builder(ItemBeingSoldDetails.this)
                        .title("Call Seller")
                        .content("This will make a phone call to the seller of this product. Call charges may apply.")
                        .canceledOnTouchOutside(false)
                        .positiveText("Call")
                        .negativeText("Cancel")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tvContact.getText().toString()));
                                if (ActivityCompat.checkSelfPermission(ItemBeingSoldDetails.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    return;
                                }
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            MaterialDialog callSeller = new MaterialDialog.Builder(ItemBeingSoldDetails.this)
                    .title("AgriCow Help")
                    .content("This mobile application helps you find cattle to buy and also to sell.")
                    .canceledOnTouchOutside(false)
                    .negativeText("Close")
                    .show();
        } else if (id == R.id.action_create_acc) {
            startActivity(new Intent(ItemBeingSoldDetails.this, CreateAccount.class));
        } else if (id == R.id.action_exit) {
            MaterialDialog dialog = new MaterialDialog.Builder(ItemBeingSoldDetails.this)
                    .title("Exit")
                    .content("Do you want to exit?")
                    .positiveText("Yes")
                    .negativeText("Cancel")
                    .canceledOnTouchOutside(false)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                            homeIntent.addCategory( Intent.CATEGORY_HOME );
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(homeIntent);
                            finish();
                        }
                    })
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(ItemBeingSoldDetails.this, ListProducts.class));
            finish();
        } else if (id == R.id.nav_add_product) {
            startActivity(new Intent(ItemBeingSoldDetails.this, AddProduct.class));
        } else if (id == R.id.nav_view_products) {
            startActivity(new Intent(ItemBeingSoldDetails.this, ListProducts.class));
        } else if (id == R.id.nav_create_account) {
            startActivity(new Intent(ItemBeingSoldDetails.this, CreateAccount.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
